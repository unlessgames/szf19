console.log("nevezes");
console.log($(".tooltipped"));
var loggedIn = true;
$('.tooltipped').tooltip({
    delay: 50,
    html: true
  });

$('#regmodal').modal({
  onOpenStart: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
    $('#reg_iframe').attr('src', 'regisztracio.html');
    console.log(modal, trigger);
  }
});
$("#logged-in").show();

$('#newentry').modal({
    onOpenStart: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
      if (trigger != undefined && trigger.dataset.nevezes_id != undefined) {
        $('#newentry_iframe').attr('src', 'nevezes/urlap_uj.php?id='+trigger.dataset.nevezes_id);
      } else {
        $('#newentry_iframe').attr('src', 'nevezes/urlap_uj.php');
      }
      $("#newentry").scrollTop(0);
      //console.log(modal, trigger);
    }
  });

$("#login").click(function() {
  ajaxLogin();
});

$("#logout").click(function() {
  ajaxLogout();
});

$("#registration").click(function() {
  $('#regmodal').modal('open');
});

$("#nevezesBTN").click(function() {
  $('#newentry').modal('open');
});

if (loggedIn) {
  $("#logged-out").hide();
  $("#logged-in").show();
  $("#session_user").text(sessionUser);
} else {
  $("#logged-out").show();
  $("#logged-in").hide();
}
// ajaxGetFormData();

var isOldIE = (navigator.userAgent.indexOf("MSIE") !== -1);
var reg_iframe_resize = iFrameResize({heightCalculationMethod: isOldIE ? 'max' : 'lowestElement', log:false, autoResize: true, checkOrigin:false},"#reg_iframe");
var reg_iframe_resize = iFrameResize({heightCalculationMethod: isOldIE ? 'max' : 'lowestElement', log:false, autoResize: true, checkOrigin:false},"#newentry_iframe");

function hideApplicationFormModal() {
  forceClose = true;
  $("#newentry").modal("close");
  $("#newentry_iframe").attr("src", "about:blank");
  ajaxGetFormData();
}

function ajaxLogin() {
  var formData = new FormData(document.getElementById("login_nevezes"));
  var oXHR = new XMLHttpRequest();
  oXHR.open("POST", "nevezes/interface.php");
  oXHR.addEventListener("load", ajaxLoginDone, false);
  oXHR.send(formData);
}

function ajaxLoginDone(e) {
  var data = JSON.parse(this.response);
  if (data.response.code == 0) {
    $("#logged-out").hide();
    $("#logged-in").show();
    $("#session_user").text(data.response.message);
    ajaxGetFormData();
  } else {
    if (data.response.code == 5 || data.response.code == 3) {
      M.toast({html: 'Hibás felhasználónév vagy jelszó!', classes: 'rounded red', displayLength:10000})
    } else alert(data.response.message);
  }
}

function ajaxLogout() {
  var formData = new FormData();
  formData.append("cmd", "logout");
  var oXHR = new XMLHttpRequest();
  oXHR.open("POST", "nevezes/interface.php");
  oXHR.addEventListener("load", ajaxLogoutDone, false);
  oXHR.send(formData);
}

function ajaxLogoutDone(e) {
  $("#logged-out").show();
  $("#logged-in").hide();
}

function ajaxGetFormData() {
  var formData = new FormData();
  formData.append("cmd", "getformdata");
  var oXHR = new XMLHttpRequest();
  oXHR.open("POST", "nevezes/interface.php");
  oXHR.addEventListener("load", ajaxGetFormDataDone, false);
  oXHR.send(formData);
}

function ajaxGetFormDataDone(e) {
  var data = JSON.parse(this.response);
  if (data.response.code == 0) {
    openforms = data.response.openforms;
    closedforms = data.response.closedforms;
    receivedforms = data.response.receivedforms;
    showForms("lezaratlan", openforms);
    showForms("lezart", closedforms);
    showForms("elfogadott", receivedforms);
    $('.tooltipped').tooltip({
        delay: 50
      });
  }
}

function ajaxDeleteForm(id) {
  if (confirm("Biztosan törlöd a nevezési lapot?")) {
    var formData = new FormData();
    formData.append("cmd", "deleteform");
    formData.append("id", id);
    var oXHR = new XMLHttpRequest();
    oXHR.open("POST", "nevezes/interface.php");
    oXHR.addEventListener("load", ajaxGetFormDataDone, false);
    oXHR.send(formData);
  }
}

function showForms(which, formarray) {
  if (formarray.length > 0) document.getElementById(which + "_container").style.display = "block";
  else document.getElementById(which + "_container").style.display = "none";
  var forms_tbody = document.getElementById(which + "_tbody");
  //var forms_header = document.getElementById(which + "_fejlec");
  //while (forms_header.nextSibling) forms_table.removeChild(forms_header.nextSibling);
  $("#"+which + "_table > tbody:last").children().remove();
  var is_form = "t";
  if (which == "lezaratlan") is_form = "f";
  for (i=0; i<formarray.length; i++) {
    var tablerow = document.createElement("tr");
    var tablecell1 = document.createElement("td");
    tablecell1.appendChild(document.createTextNode((i+1) + ": " + formarray[i].cim));
    tablerow.appendChild(tablecell1);
    var tablecell2 = document.createElement("td");
    var k=0;
    for (j=0; j<formarray[i].files.length; j++) {
      if (formarray[i].files[j].is_form == is_form) {
        if (k++ != 0) {
          var dividespan = document.createElement("span");
          dividespan.className = "spacer";
          dividespan.appendChild(document.createTextNode("|"));
          tablecell2.appendChild(dividespan);
        }
        tablecell2.appendChild(document.createTextNode(formarray[i].files[j].fname));
      }
    }
    tablerow.appendChild(tablecell2);
    var tablecell3 = document.createElement("td");
    tablecell3.className = "center-align";
    if (which == "lezaratlan") {
      var modifybutton = document.createElement("a");
      modifybutton.className = "btn-floating tooltipped blue modal-trigger";
      modifybutton.setAttribute("data-position", "right");
      modifybutton.setAttribute("data-tooltip", "Módosítás");
      modifybutton.setAttribute("data-delay", "50");
      modifybutton.setAttribute("href", "#newentry");
      modifybutton.setAttribute("data-nevezes_id", formarray[i].nevezes_id);
      var modifybuttoncontent = document.createElement("i");
      modifybuttoncontent.className = "material-icons left";
      modifybuttoncontent.innerHTML = "mode_edit";
      modifybutton.appendChild(modifybuttoncontent);
      tablecell3.appendChild(modifybutton);
      var deletebutton = document.createElement("a");
      deletebutton.className = "btn-floating tooltipped red";
      deletebutton.setAttribute("data-position", "right");
      deletebutton.setAttribute("data-tooltip", "Törlés");
      deletebutton.setAttribute("data-delay", "50");
      deletebutton.href = "javascript:ajaxDeleteForm(" + formarray[i].nevezes_id + ")";
      var deletebuttoncontent = document.createElement("i");
      deletebuttoncontent.className = "material-icons left";
      deletebuttoncontent.innerHTML = "delete";
      deletebutton.appendChild(deletebuttoncontent);
      tablecell3.appendChild(deletebutton);
    } else if (which == "lezart") {
      var printbutton = document.createElement("a");
      printbutton.className = "btn-floating tooltipped blue";
      printbutton.setAttribute("data-position", "right");
      printbutton.setAttribute("data-tooltip", "Nyomtatás");
      printbutton.setAttribute("data-delay", "50");
      printbutton.href = "nevezes/nyomtatas.php?id=" + formarray[i].nevezes_id;
      printbutton.target = "_blank";
      var printbuttoncontent = document.createElement("i");
      printbuttoncontent.className = "material-icons left";
      printbuttoncontent.innerHTML = "print";
      printbutton.appendChild(printbuttoncontent);
      tablecell3.appendChild(printbutton);
      var formuploadbutton = document.createElement("a");
      formuploadbutton.className = "btn-floating tooltipped blue";
      formuploadbutton.setAttribute("data-position", "right");
      formuploadbutton.setAttribute("data-tooltip", "Feltöltés");
      formuploadbutton.setAttribute("data-delay", "50");
      formuploadbutton.href = "javascript:;";
      formuploadbutton.setAttribute("data-nevezes_id", formarray[i].nevezes_id);
      formuploadbutton.onclick = function() {
        selectFile(this);
        return false;
      }
      var formuploadbuttoncontent = document.createElement("i");
      formuploadbuttoncontent.className = "material-icons left";
      formuploadbuttoncontent.innerHTML = "input";
      formuploadbutton.appendChild(formuploadbuttoncontent);
      tablecell3.appendChild(formuploadbutton);
    }
    tablerow.appendChild(tablecell3);
    forms_tbody.appendChild(tablerow);
  }
}

function selectFile(theLink) {
  document.getElementById("upFile").addEventListener("change", handleFileSelect, false);
  document.getElementById("upFile").sender = theLink;
  document.getElementById("upFile").click();
}

function handleFileSelect(e) {
  var uFile = e.target.files[0];
  if (uFile.size > 10485760) {
    alert("Túl nagy fájl (>10M)!");
    return;
  }
  document.getElementById("progressbar_div").style.display = "block";
  uploadFile(e);
}

function uploadFile(e) {
  var uFile = e.target.files[0];
  var formData = new FormData();
  formData.append("cmd", "uploadform");
  formData.append("id", $(e.target.sender).data("nevezes_id"));
  formData.append("uploadfile", uFile, uFile.name);
  var oXHR = new XMLHttpRequest();
  oXHR.upload.filename = uFile.name;
  oXHR.upload.addEventListener("progress", uploadProgress , false);
  oXHR.addEventListener("load", uploadFinish, false);
  oXHR.addEventListener("error", uploadError, false);
  oXHR.addEventListener("abort", uploadAbort, false);
  oXHR.open("POST", "nevezes/interface.php");
  oXHR.send(formData);
}

function uploadProgress(e) {
  if (e.lengthComputable) {
    var val = Math.round((e.loaded/e.total) * 100);
    $('.progress-bar').css('width', val+'%').attr('aria-valuenow', val);
  }
}

function uploadFinish() {
  $('.progress-bar').css('width', '0%').attr('aria-valuenow', 0);
  document.getElementById("progressbar_div").style.display = "none";
  var data = JSON.parse(this.response);
  if (data.response.code == 0) {
    ajaxGetFormData();
  } else {
    uploadError();
  }
}

function uploadError() {
  alert("uploadError");
}

function uploadAbort() {
  alert("uploadAbort");
}
