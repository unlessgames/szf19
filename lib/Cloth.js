/*
 * Cloth Simulation using a relaxed constraints solver
 */

// Suggested Readings

// Advanced Character Physics by Thomas Jakobsen Character
// http://freespace.virgin.net/hugo.elias/models/m_cloth.htm
// http://en.wikipedia.org/wiki/Cloth_modeling
// http://cg.alexandra.dk/tag/spring-mass-system/
// Real-time Cloth Animation http://www.darwin3d.com/gamedev/articles/col0599.pdf

var DAMPING = 0.03;
var DRAG = 1 - DAMPING;
var MASS = 0.1;
var GRAVITY = 981 * 1.4;

var TIMESTEP = 18 / 1000
var TIMESTEP_SQ = TIMESTEP * TIMESTEP


function Particle( x, y, z, mass, fun ) {

	this.position = new THREE.Vector3();
	this.previous = new THREE.Vector3();
	this.original = new THREE.Vector3();
	this.a = new THREE.Vector3( 0, 0, 0 ); // acceleration
	this.mass = mass;
	this.invMass = 1 / mass;
	this.tmp = new THREE.Vector3();
	this.tmp2 = new THREE.Vector3();

	// init

	fun( x, y, this.position ); // position
	fun( x, y, this.previous ); // previous
	fun( x, y, this.original );

}

// Force -> Acceleration

Particle.prototype.addForce = function ( force ) {

	this.a.add(
		this.tmp2.copy( force ).multiplyScalar( this.invMass )
	);

};


// Performs Verlet integration

Particle.prototype.integrate = function ( timesq ) {

	var newPos = this.tmp.subVectors( this.position, this.previous );
	newPos.multiplyScalar( DRAG ).add( this.position );
	newPos.add( this.a.multiplyScalar( timesq ) );

	this.tmp = this.previous;
	this.previous = this.position;
	this.position = newPos;

	this.a.set( 0, 0, 0 );

};


var diff = new THREE.Vector3();

function satisfyConstraints( p1, p2, distance ) {

	diff.subVectors( p2.position, p1.position );
	var currentDist = diff.length();
	if ( currentDist === 0 ) return; // prevents division by 0
	var correction = diff.multiplyScalar( 1 - distance / currentDist );
	var correctionHalf = correction.multiplyScalar( 0.5 );
	p1.position.add( correctionHalf );
	p2.position.sub( correctionHalf );

}


function Cloth( w, h, restDistance, clothFunction, gravity, pins, geometry) {

	w = w || 10;
	h = h || 10;
	this.w = w;
	this.h = h;
	this.pins = pins
	this.gravity = gravity
	this.restDistance = restDistance
	this.wind = {
		enabled : true,
		strength : 2,
		force : new THREE.Vector3(0,0,0)
	}
	this.geometry = geometry



	this.tmpForce = new THREE.Vector3();

	this.lastTime = 0;

	var particles = [];
	var constraints = [];

	var u, v;

	// Create particles
	for ( v = 0; v <= h; v ++ ) {

		for ( u = 0; u <= w; u ++ ) {

			particles.push(
				new Particle( u / w, v / h, 0, MASS, clothFunction )
			);

		}

	}

	// Structural

	for ( v = 0; v < h; v ++ ) {

		for ( u = 0; u < w; u ++ ) {

			constraints.push( [
				particles[ index( u, v ) ],
				particles[ index( u, v + 1 ) ],
				restDistance
			] );

			constraints.push( [
				particles[ index( u, v ) ],
				particles[ index( u + 1, v ) ],
				restDistance
			] );

		}

	}

	for ( u = w, v = 0; v < h; v ++ ) {

		constraints.push( [
			particles[ index( u, v ) ],
			particles[ index( u, v + 1 ) ],
			restDistance

		] );

	}

	for ( v = h, u = 0; u < w; u ++ ) {

		constraints.push( [
			particles[ index( u, v ) ],
			particles[ index( u + 1, v ) ],
			restDistance
		] );

	}

	this.particles = particles;
	this.constraints = constraints;

	function index( u, v ) {

		return u + v * ( w + 1 );

	}

	this.index = index;

	this.simulate = function ( time ) {

		if ( ! this.lastTime ) {

			this.lastTime = time;
			return;

		}

		var i, il, particles, particle, pt, constraints, constraint;

		// Aerodynamics forces

		if ( this.wind.enabled ) {

			var indx;
			var normal = new THREE.Vector3();
			var indices = this.geometry.index;
			var normals = this.geometry.attributes.normal;

			particles = this.particles;

			for ( i = 0, il = indices.count; i < il; i += 3 ) {

				for ( j = 0; j < 3; j ++ ) {

					indx = indices.getX( i + j );
					normal.fromBufferAttribute( normals, indx )
					this.tmpForce.copy( normal ).normalize().multiplyScalar( normal.dot( this.wind.force ) );
					particles[ indx ].addForce( this.tmpForce );

				}

			}

		}

		for ( particles = this.particles, i = 0, il = particles.length; i < il; i ++ ) {

			particle = particles[ i ];
			particle.addForce( this.gravity );

			particle.integrate( TIMESTEP_SQ );

		}

		// Start Constraints

		constraints = this.constraints;
		il = constraints.length;

		for ( i = 0; i < il; i ++ ) {

			constraint = constraints[ i ];
			satisfyConstraints( constraint[ 0 ], constraint[ 1 ], constraint[ 2 ] );

		}


		// Pin Constraints

		for ( i = 0, il = this.pins.length; i < il; i ++ ) {

			var xy = this.pins[ i ];
			var p = particles[ xy ];
			p.position.copy( p.original );
			p.previous.copy( p.original );

		}
	}
}

