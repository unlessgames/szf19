Main = 
  is_mobile : false
  resize : () ->
    @dom.resize_page()
    @world.resize()
  init : () ->
    @dom = new Domer(@)
    @toner = Toner
    @toner.init()

    if not WEBGL.isWebGLAvailable()
      document.body.appendChild( WEBGL.getWebGLErrorMessage())
    else
      @world = World
      @world.init(@dom)
    $(window).resize @resize.bind(@)
  open : (id) ->
    @dom.open_page id
    @world.show_obj id

Main.init()
