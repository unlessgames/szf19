IDLE = 0
RUNNING = 1
ENDOFCYCLE = -1

class Clock
  constructor : (@speed = 1) ->
    @time = 0
    @eoc = false
    @cycles = 0
  update : (dt) ->
    nt = @time + @speed * dt
    fnt = Math.floor nt
    @time = nt - fnt
    if fnt > 0
      @eoc = true
      @cycles++
    else
      @eoc = false
    @eoc

  reset : () ->
    @time = 0
    @eoc = false
    @cycles = 0

class ShotClock
  constructor : (@speed = 1) ->
    @clock = new Clock @speed
    @state = 0
    @time = 0
  update : (dt) ->
    if @state is 1
      if @clock.update dt
        @time = 1
        @state = ENDOFCYCLE
      else
        @time = @clock.time
        @state = RUNNING
    else
      @state = IDLE
    @state
  start : () ->
    @time = 0
    @clock.reset()
    @state = RUNNING
    # @running = true

###

clock
curves
midi

###

_bind = (o, f) -> o[f].bind o
clamp = (x, a, b) -> if x < a then a else if x > b then b else x
_now = (s = 1) -> new Date().getTime() * s
lerp = (a, b, t) -> a*(1-t)+b*t
ilerp = (a, b, p) -> p = clamp(p, a, b); (p - a) / (b - a)
ilerpi = (a, b, p) -> 
  r = ilerp(a, b, p)
  if r is 1 then 0.9999 else r
lerpa = (a, b, t) -> a.map((_a, i) -> lerp(_a, b[i], t))
lerpl = (a, b, t) ->
  for v, i in a.length
    lerp v, b[i], t

now = () -> new Date().getTime()
ranfrom = (a) -> a[Math.floor(Math.random() * a.length)]
wrap = (i, arr) ->
  if i >= arr.length then i % arr.length
  else if i < 0 then arr.length - (Math.abs(i) % arr.length)
  else i


class Gradient
  constructor : (@colors) ->
  at : (t, hsl) ->
    if t > 1 then t -= Math.floor t
    if t is 1 then t = 0.999
    if hsl
      new T.Color().copy(@colors[0]).lerpHSL(@colors[1], t)
    else
      new T.Color().copy(@colors[0]).lerp(@colors[1], t)
