// Generated by CoffeeScript 2.4.1
var Domer;

Domer = (function() {
  class Domer {
    constructor(main) {
      this.main = main;
      this.iframe = $("<iframe>", {
        class: "iframe"
      }).attr("scrolling", "no").get(0);
      $("#page").appendTo($("body"));
      $("#close").on("mousedown", () => {
        return this.close_page();
      });
      $("#previous").click(() => {
        return this.step_pages(-1);
      });
      $("#next").click(() => {
        return this.step_pages(1);
      });
      $("#left, #title").click(() => {
        if (this.current == null) {
          return this.open_page("verseny");
        } else if (this.current.id === "nyertesek") {
          this.current_index = 0;
          return this.step_pages(0);
        }
      });
      $(".simplebar-content").html(this.iframe);
      this.inside_nyertes = false;
    }

    hover_page(p) {
      if ((p != null) && (this.current == null)) {
        $("#title").html(p.title);
        // $("#left").css(display : "inline-block")
        return $("#left").html(p.button);
      }
    }

    // else
    //   $("#left").css display : "none"
    set_title(p) {
      if (this.current != null) {
        $("#title").html(p.title);
        return $("#left").html(p.button);
      }
    }

    resize_page() {
      var body, doc, h, html;
      doc = this.iframe.contentWindow.document;
      body = doc.body;
      html = doc.documentElement;
      h = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
      // h = @iframe.contentDocument.getElementsByClassName("pages")[0].scrollHeight
      h = {
        height: `${h + (isMobile.any ? 100 : 10)}px`
      };
      $("iframe").css(h);
      return $(".simplebar-content-wrapper").css(h);
    }

    show_page() {
      return $("#page").animate({
        opacity: 1,
        top: $("#titlebar").height()
      }, 320);
    }

    active(n) {
      return (this.current != null) && this.current.id === n;
    }

    open_url(url, menu) {
      if ((menu != null) || (this.current == null)) {
        return this.open_page((menu != null ? menu : "nevezes"), false, url);
      } else {
        $(this.iframe).attr("src", url);
        $(".simplebar-content-wrapper").scrollTop(0);
        $("iframe").css({
          height: 0
        });
        return this.iframe.onload = (() => {
          return this.resize_page();
        });
      }
    }

    open_page(n, from_step, src) {
      var p, w;
      p = get_page(n);
      $("#title").html(p.title);
      $("#left").css({
        display: "inline-block"
      }).html(p.button);
      this.current = p;
      this.current_index = Pages.findIndex(function(pp) {
        return pp.id === p.id;
      });
      if (p.id === "nyertesek") {
        this.current_index = 0;
      }
      if (src == null) {
        src = p.subpages != null ? p.subpages[0] : p.source;
      }
      $(this.iframe).attr("src", src);
      this.set_title(p);
      if (innerWidth / innerHeight < 1 || isMobile.any) {
        $("#page").css({
          top: 0,
          width: "100vw",
          left: 0,
          display: "block"
        });
      } else {
        w = 100 * Objects[p.id].page.w;
        $("#page").css({
          top: "100vh",
          width: `${w}vw`,
          display: "block"
        });
      }
      $(".simplebar-content-wrapper").scrollTop(0);
      $("iframe").css({
        height: 0
      });
      this.iframe.onload = (() => {
        return this.resize_page();
      });
      this.show_page();
      if (!from_step) {
        return Main.world.show_obj(p.id);
      }
    }

    close_page(cb, keep_bar) {
      if (this.current.id === "nyertesek") {
        this.current_index = 0;
      }
      if (this.current && (Objects[this.current.id].exit != null)) {
        Objects[this.current.id].exit();
      }
      this.current = null;
      $("#page").animate({
        opacity: 0,
        top: "100vh"
      }, 420, function(e) {
        $(this).css({
          width: 0,
          display: "none"
        });
        if (cb != null) {
          return cb(e);
        }
      });
      if (!keep_bar) {
        return Main.world.show_obj("menu");
      }
    }

    step_pages(d) {
      var next, src;
      if (Main.world.cam_clock.state === 0) {
        if (this.current != null) {
          if (this.current.id === "nyertesek") {
            this.current_index = wrap(this.current_index + d, this.current.subpages);
            src = this.current.subpages[this.current_index];
            return $(this.iframe).attr("src", src);
          } else {
            next = Pages[wrap(this.current_index + d, Pages)];
            Main.world.show_obj(next.id, true);
            return this.close_page((() => {
              return this.open_page(next.id, true);
            }), true);
          }
        } else {
          this.open_page("verseny");
          return Main.world.show_obj("verseny");
        }
      }
    }

    random_content() {
      return new Array(100).fill(0).map(function() {
        return Math.ranDom() * 1000000;
      }).join("<br>");
    }

    update(dt, hit, h) {
      if ((hit != null) && (hit.object.name[0] !== "-" && (NYERTESEK || hit.object.name !== "_nyertesek"))) {
        $("#main").css({
          cursor: "pointer"
        });
      } else {
        $("#main").css({
          cursor: "default"
        });
      }
      if (h != null) {
        if (this.last !== h) {
          this.hover_page(get_page(h));
        }
      } else {
        if ((this.current == null) && Main.world.cam_clock.state === 0) {
          $("#title").html(Title);
          // $("#left").css display : "none"
          $("#left").html("<19");
        }
      }
      return this.last = h;
    }

  };

  Domer.prototype.last = null;

  return Domer;

}).call(this);

//# sourceMappingURL=domer.js.map
