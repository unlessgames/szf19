var Page = class Page {
  constructor(id, title, button, source, subpages) {
    this.id = id;
    this.title = title;
    this.button = button;
    this.source = source;
    this.subpages = subpages;
  }
};
var LogoImage = class LogoImage {
  constructor(name, x, y, size = 1, c) {
    this.base_dir = "assets/logo";
    this.name = name;
    this.x = x;
    this.y = y;
    this.size = size;
    this.color = c;
  }

};

var Pages = [
  new Page("verseny", "verseny", "!", "pages/verseny.html"),
  new Page("gyik", "gy.i.k.", "?", "pages/gyik.html"),
  new Page("nevezes", "nevezés", "+", "pages/nevezes2.html"),
  new Page("kapcsolat", "kapcsolat", "@", "pages/kapcsolat.html"),
  new Page("szponzorok", "támogatók és partnerek", "&", "pages/szponzorok.html"),
  new Page("esemenyek", "kiállítás", "~", "pages/esemenyek.html"),
  new Page("korabbiak", "korábbiak", "\\\\", "pages/korabbiak.html")
];

var Nyertesek = new Page("nyertesek", "nyertesek", "*", "", [
    "pages/nyertesek.html",
    "nyertesek/gf.html",
    "nyertesek/bf.html",
    "nyertesek/jm.html",
    "pages/szponzorok.html"
  ])

var NYERTESEK = true;

var Logos = [
  new LogoImage("logo_szamlazz.hu.svg", 0.15, 0.15, 0.7), 
  new LogoImage("logo_guidance.svg", 0.1, 0.35, 0.85), 
  new LogoImage("logo_ntp.svg", 0, 0.36), 
  new LogoImage("logo_goethe.svg", 0, 0.25), 
  new LogoImage("logo_ms.svg", 0.07, 0.38, 0.9)
];

var Title = "szabadfogású számítógép verseny";
