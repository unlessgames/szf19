World = 
  lasttime : 0
  raycaster : new T.Raycaster()
  cam_clock : new ShotClock(2)

  look_towards_mouse : (delta) ->
    xdist = 0.2 + (innerHeight / innerWidth) * 0.4
    ydist = 0.1
    t = 0.1
    p = T.Math.lerp(@camera.parent.parent.rotation.y, Objects.world.cam.pitch + 0.1 - Mouse._x * xdist, t)
    
    ya = T.Math.lerp(@camera.parent.rotation.x, Objects.world.cam.yaw - ydist * 0.5 + Mouse._y * ydist, t)
    if not isNaN(p) and not isNaN(ya)
      @camera.parent.parent.rotation.set 0, p, 0 
      @camera.parent.rotation.set ya, 0, 0

  init_renderer : () ->
    @renderer = new T.WebGLRenderer(antialias: false)
    @renderer.shadowMap.enabled = true;
    @renderer.setPixelRatio window.devicePixelRatio
    @renderer.setSize window.innerWidth, window.innerHeight
    @renderer.gammaOutput = true
    @renderer.gammaFactor = 2.2

  switch_cam : () ->
    @camera = if @camera.name is "dollycam" then @pilot.camera else @dollycam
  default_view : () ->
    pos : Objects.world.cam.pos.clone()
    pitch : Objects.world.cam.pitch
    yaw : Objects.world.cam.yaw
    view_offset : 0
  current_view : (from_step) ->
    vo =
    pos : @pilot.controls.getObject().position.clone()
    pitch : @pilot.controls.getObject().rotation.y
    yaw : @pilot.controls.getObject().children[0].rotation.x
    view_offset : if not from_step
                    @pilot.camera.view.offsetX / innerWidth
                  else 
                    p = Objects[@dom.current.id].page
                    (if p.anchor < 0.5 then -1 else 1 ) * (1 - p.w) * 0.5
  reset_pilot : () ->
    @pilot.fov = 75.31687780932562
    @pilot.camera.fov = @pilot.fov 
    @pilot.camera.updateProjectionMatrix()
    @target = @default_view()
    @last = @target
    @cam_clock.start()

  show_obj : (n, from_step) ->
    if n is "menu"
      @last = @target
      @target = @default_view()
    else if n?
      @last = @current_view from_step
      p = Objects[n].page
      next = 
        pos : Objects[n].cam.pos.clone()
        pitch : Objects[n].cam.pitch
        yaw : Objects[n].cam.yaw
        view_offset : (if p.anchor < 0.5 then -1 else 1 ) * (1 - p.w) * 0.5
      @target = next

      if not isMobile.any and not Objects.mute.muted and Objects[n].instrument
        Objects[n].instrument.mute = false

    else
      l = @last
      @last = @target
      @target = l

    @cam_clock.start()

  set_view_offset : (x = 0, y = 0) ->
    w = innerWidth
    h = innerHeight
    @pilot.camera.setViewOffset( 
      w, h, x, y, w, h )

  lerp_cam : (a, b, t) ->
    @camera.parent.parent.position.lerpVectors(a.pos, b.pos, t)
    @camera.parent.parent.rotation.set 0, lerp(a.pitch, b.pitch, t), 0
    @camera.parent.rotation.set lerp(a.yaw, b.yaw, t), 0, 0
    @camera.updateProjectionMatrix()
    w = innerWidth
    h = innerHeight
    # console.log "a : #{a.view_offset}\n b : #{b.view_offset}\n t : #{t}"
    @set_view_offset(lerp(w * a.view_offset, w * b.view_offset, t))

  closest_hit : (hs) ->
    if hs.length > 0
      hs = hs.sort((a, b) -> a.distance > b.distance)
      if Dom? and Main.dom.active? and not Main.dom.active hs[0].object.name.substring(1)
        hs[0]
      else 
        null
    else null


  check_hover : (hit) ->
    nt = null
    if hit?
      nt = @find_hovered hit.object.name
      if nt?
        if nt isnt @hovered
          @hover_obj nt
          if @hovered?
            @leave_obj @hovered

    if !nt? and @hovered?
      @leave_obj @hovered
      @hovered = null
    else
      @hovered = nt
    @hovered

  reset_mat : (obj) ->
    obj.object.material = 
      if obj.emissive?
        Mats.emissive
      else 
        Mats.main

  hover_obj : (n) ->
    if not Main.dom.active n# or (NYERTESEK or n isnt "nyertesek")
      obj = Objects[n]
      if obj.hover?
        obj.hover()
      else
        obj.object.material = Mats.hover

  leave_obj : (n) ->
    if not Main.dom.active n
      obj = Objects[n]
      if obj.leave?
        obj.leave()
      else
        @reset_mat obj

  find_hovered : (name) ->
    if name[0] is "_"
      name = name.substring(1)
    n = Object.keys(Objects).find((k) -> 
      if Objects[k].hovers?
        Objects[k].hovers.indexOf(name) isnt -1
      else
        k is name
      )
    if n? then n else null

  init_obj : (child, raygroup) ->
    name = child.name
    o = Objects[name]
    if o?
      child.material =
        if o.emissive
          Mats.emissive
        else
          Mats.main
      if not o.emissive
        child.receiveShadow = true
        child.castShadow = true
    else  
      if name[0] is "_"
        child.material = Mats.hidden
        @raygroup.push child
      else if name[0] is "-"
        child.material = Mats.hidden
        if name[0] is "-eyetarget"
          @raygroup.push child
      else
        if name is "mute_box" and not isMobile.any
          @raygroup.push child
        child.material = Mats.main
        child.receiveShadow = true
        child.castShadow = true
    child

  click_obj : (h) ->
    if h? and not (@dom.current? and @dom.current.id is "nyertesek")
      n = h.object.name
      if n[0] is "_"
        n = n.substring(1)
      else if n is "mute_box" and not isMobile.any
        n = "mute"

      if Objects[n]?
        obj = Objects[n]
        if not obj.static and get_page(n)
          if not Main.dom.active n and @cam_clock.state is IDLE
            p = get_page n
            @leave_obj n
            Main.dom.open_page n
        if obj.click?
          obj.click()

  resize : () ->
    @renderer.setSize  window.innerWidth, window.innerHeight
    @pilot.camera.aspect = $("#gl").width() / window.innerHeight
    @pilot.camera.updateProjectionMatrix()

  scroll : (e) ->

  init_objects : (s) ->
    for k in Object.keys(Objects)
      Objects[k].init s

  update_objects : (dt, ht) ->
    for k in Object.keys(Objects)
      obj = Objects[k]
      if obj.update then obj.update dt, ht

  init : (dom) ->
    @dom = dom
    @init_renderer()
    console.log("%c unless games ", "background: #444; color: #eee")
    $("<div>", id : "main", html : [@renderer.domElement]).appendTo $("body")
    @renderer.domElement.id = "gl"


    
    $("#gl").click(() => @click_obj @hit).attr("touch-action", "none")
    $(document).bind "mousewheel", (e) => @scroll(e)

    # stats = new Stats()
    # {stats : stats}

    Mats.init()
    @scene = new T.Scene()

    loaders.gltf.load( 'map.gltf', ( gltf ) =>
      @raygroup = []
      gltf.scene.traverse (c) => @init_obj(c, @raygroup)
      gltf.scene.name = "objects"
      @scene.add gltf.scene
      @init_objects @scene
      @loaded = true
    )
    Keys.init()
    Mouse.init()
    @pilot = new Pilot @scene
    
    @camera = @pilot.camera
    Keys.listen @, "switch_cam", "press", "KeyE"
    
    @reset_pilot()
    @update()
  not_muted : () -> not isMobile.any and not Objects.mute.muted
  update : () ->
    requestAnimationFrame @update.bind(@)
    # site.stats.update()
    now = _now()
    dt = (now - @lasttime) * 0.001
    @lasttime = now

    if @loaded
      @raycaster.setFromCamera {x : Mouse._x, y : Mouse._y}, @camera
      targets = 
        if @dom.current? and @not_muted()
          @raygroup.filter((r) => r.name isnt "_"+@dom.current.id).concat(Objects[@dom.current.id].raygroup)
        else
          @raygroup
      hits = @raycaster.intersectObjects targets, true
      @hit = @closest_hit hits


      if @cam_clock.update(dt) isnt IDLE
        @lerp_cam(@last, @target, @cam_clock.time * @cam_clock.time)
      else if @dom?
        if not @dom.current?
          @look_towards_mouse dt
        else 
          @pilot.update dt
          if @not_muted()
            Objects[@dom.current.id].mouse dt, @hit

      hovered_name = @check_hover(
        if NYERTESEK and Objects.nyertesek.opened then null
        else @hit
      )

      if @dom? and @dom.update? and @cam_clock.state is IDLE
        @dom.update dt, @hit, hovered_name

      @update_objects dt, @hit

      @renderer.render @scene, @camera
