Dom = {}

loaded = false



get_page = (n) -> 
  p = if n is "nyertesek" then Nyertesek else Pages.find((p) -> p.id is n)

  
Colors = 
  page : "#999"
  inner : "#aaa"
  sunshine : 0xe7c36a
  ambient : 0x42331a
  light_blue : 0x26ABE2
  red : 0xA63C1C
  yellow : 0xFFC642
  grey : 0x666360
  black : new T.Color(0.1,0.09,0.08)

loaders = 
  gltf : new T.GLTFLoader().setPath("assets/")
  tex : new T.TextureLoader().setPath("assets/")
  font : new T.FontLoader().setPath "assets/"

Mats =
  main : new T.MeshLambertMaterial()
  hidden : new T.MeshBasicMaterial(visible : false)
  emissive : new T.MeshBasicMaterial(side : T.DoubleSide)
  sun : new T.MeshBasicMaterial(side : T.DoubleSide)
  hover : new T.MeshLambertMaterial()#color : new T.Color 0xf7e35a)
  white : new T.MeshLambertMaterial()
  logos : new T.MeshBasicMaterial(transparent : true)
  letters : new T.MeshBasicMaterial(color : new T.Color 0xf7e35a)
  info : new T.MeshBasicMaterial(transparent : true)
  init : () ->
    @info_tex = loaders.tex.load "info.png"
    @info_tex.magFilter = T.NearestFilter
    @info_tex.minFilter = T.NearestFilter
    @info.map = @info_tex
    @info.color.setHex 0xeeeddd
    @info.opacity = 0.8
    
    @logo_tex = new T.CanvasTexture $("#logos").get(0).getContext("2d").canvas
    @logo_tex.minFilter = T.NearestFilter
    @logo_tex.magFilter = T.NearestFilter
    # @logo_tex.encoding = T.sRGBEncoding
    @logos.map = @logo_tex
    @logos.side = T.DoubleSide

    @tex = loaders.tex.load "colors.png"
    @tex.magFilter = T.NearestFilter;
    @tex.minFilter = T.LinearMipMapLinearFilter;
    @tex.wrapS = T.RepeatWrapping;
    @tex.wrapT = T.RepeatWrapping;
    @tex.repeat.y = -1
    @main.map = @tex
    @emissive.map = @tex
    @emissive.color.setHSL 0.1, 0.1, 0.8

class PageTransform
  constructor : (@anchor = 0, @x = 0.1, @w = 0.56, @h = 1) ->

Objects = 
  korabbiak : 
    page : new PageTransform(0, 0.6, 0.4)
    cam : {"pos":[-0.948120356820472,2.827389391961688,6.059465371741808],"pitch":0.5679999999999991,"yaw":-0.07000000000000026,"roll":0,"fov":14.036601356639087}

    ferris : (p, r, name = "korabbiak") ->
      o = new T.Object3D()
      o.name = name

      block = (_p, _r, x, y, z, highlight = false) ->
        g = new T.BoxGeometry(x, y, z)
        g.faceVertexUvs.map((uv) -> uv.map((u) -> u.map((_u) -> _u.set 0.9, 0.4)))
        s = new T.Mesh(g, Mats.main)
        # s.name = name
        s.highlight = highlight
        s.position.set _p.x, _p.y, _p.z
        s.rotation.set _r.x, _r.y, _r.z
        s.receiveShadow = true
        s.castShadow = true
        s
      hinge = (b, i) ->
        b.seat = true
        b.step_id = i
        h = new T.Object3D()
        h.position.copy b.position
        h.add b
        b.position.set 0,0,0
        b.translateOnAxis new Vector3(0,0,1), 0.15
        h

      count = 16
      radius = 1.45

      @wheel = new T.Group()
      
      for c in [0..count-1]
        t = c / count
        phase = t * Math.PI * 2
        x = Math.sin(phase) * radius
        y = Math.cos(phase) * radius

        _phase = phase + Math.PI * 2 * (1 / count) * 0.5
        _x = Math.sin(_phase) * radius * 1
        _y = Math.cos(_phase) * radius * 1
        seat = hinge block(
            new Vector3(x, y, 0),
            new Vector3(0, 0, -phase), 0.3, 0.3, 0.45
          ), c
        @raygroup.push seat
        @wheel.add(
          seat
          block(
            new Vector3(x * 0.5, y * 0.5, 0),
            new Vector3(0, 0, -phase), 0.05, radius, 0.05, true
          )
          block(
            new Vector3(_x, _y, 0),
            new Vector3(0, 0, -_phase + Math.PI * 0.5), 0.05, 7.1 * radius * (1 / count), 0.05
          )
        )
        @seats.push seat
      o.position.set p.x, p.y, p.z
      o.rotation.set r.x, r.y, r.z
      o.add @wheel

      pw = 0.08
      lx = 0.4
      ly = -radius * 0.57
      lz = 0.42
      @base = new T.Group()
      pi15 = Math.PI * 0.15
      @base.add block(
        new Vector3(0,0,0)
        new Vector3(Math.PI * 0.5, Math.PI * 0.25, 0), 0.1, 1, 0.1
      )
      @base.add block(
        new Vector3(lx,ly,lz)
        new Vector3(0, 0, pi15), pw, radius * 1.2, pw
      )
      @base.add block(
        new Vector3(-lx,ly,lz)
        new Vector3(0, 0, -pi15), pw, radius * 1.2, pw
      )


      @base.add block(
        new Vector3(lx,ly,-lz)
        new Vector3(0, 0, pi15), pw, radius * 1.2, pw
      )
      @base.add block(
        new Vector3(-lx,ly,-lz)
        new Vector3(0, 0, -pi15), pw, radius * 1.2, pw
      )


      @base.add block(
        new Vector3(0, -radius * 1.08, 0.42)
        new Vector3(0,0,0), 1.755, 0.21, 0.15
      )

      @base.add block(
        new Vector3(0, -radius * 1.08, -0.42)
        new Vector3(0,0,0), 1.755, 0.21, 0.15
      )

      o.add @base
      o
    start_scale : 0.8
    seats : []
    init : (s) ->
      @bounds = s.getObjectByName "_korabbiak"
      @object = @ferris(@bounds.position.clone(), new T.Vector3(0,Math.PI*0.5 - 0.5,0))
      # @object.traverse((c) -> c.name = "korabbiak")
      @object.traverse((c) -> c.material = Mats.main) #TODO use texture
      s.add @object
      @wheel.scale.set @start_scale, @start_scale, @start_scale
      @wheel.traverse((c) => if c.seat then @set_seat_uv(c))
      @instrument = Toner.chords
    update : (dt, hit) ->
      if Tone.Transport.context.state is "suspended"
        @wheel.rotateZ(-dt * 0.3)
      else
        bpm = Tone.Transport.bpm.value
        bl = (60 / bpm) * 32
        sec = Tone.Transport.getSecondsAtTime() / bl

        @wheel.rotation.set(@wheel.rotation.x, @wheel.rotation.y, sec * Math.PI * 2)


      v = new Vector3()
      for c in @seats
        c.updateMatrixWorld()
        v.setFromMatrixPosition( c.matrixWorld )
        v.setY v.y - 1
        c.lookAt v
        c.children[0].rotateZ(dt * 0.5)

    hovers : ["korabbiak"]
    leave : () ->
      @wheel.scale.set @start_scale, @start_scale, @start_scale
      @wheel.traverse((c) -> if c.highlight then c.material = Mats.main)
    hover : () ->
      s = 0.9
      @wheel.scale.set s,s,s
      Mats.hover.color.setHex Colors.grey
      @wheel.traverse((c) ->  if c.highlight then c.material = Mats.hover)

    set_seat_uv : (c) ->
      s = 1 / 8
      uvc = x : 0.9, y : 0.4
      if Toner.chords.pattern[c.step_id].note isnt Toner.chords.notes.length
        uvc.x = Toner.chords.pattern[c.step_id].note * s
        uvc.y = 0
      c.geometry.faceVertexUvs.map((uv) -> uv.map((u) -> u.map((_u) -> _u.set uvc.x, uvc.y)))
      c.geometry.uvsNeedUpdate = true
    pressed : false
    mouse : (dt, hit) ->
      if hit? and hit.object.seat
        if Mouse.pressed() and not @pressed
          @pressed = true
          l = Toner.chords.notes.length + 1
          Toner.chords.pattern[hit.object.step_id].note = 
            if Mouse.pressed(2) then l - 1
            else (Toner.chords.pattern[hit.object.step_id].note + 1) % l
          @set_seat_uv hit.object
      if not Mouse.pressed() and @pressed
        @pressed = false

  verseny : 
    page : new PageTransform()
    cam : {"pos":[0.964879217585516,3.039314275714484,1.0542075596146336],"pitch":1.0895799999999995,"yaw":0.049269999999999814,"roll":0,"fov":15.622524275517366}

    init : (s) ->
      @logo = s.getObjectByName "verseny_logo"
      @logo.material = Mats.emissive
      @ring = s.getObjectByName "verseny_ring"
      @ring.material = Mats.emissive
      @inner = s.getObjectByName "verseny"
      @top = s.getObjectByName "verseny_top"
      @bottom = s.getObjectByName "verseny_bottom"
      @windows = s.getObjectByName "verseny_windows"
      @door = s.getObjectByName "verseny_door"
      @windows.material = Mats.logos
      @pole = s.getObjectByName "verseny_pole"
      @start_rot = @inner.quaternion.clone()
      @raygroup = [@windows, @door]
      @instrument = Toner.bass
    update : (dt, hit) ->
      @logo.rotateOnAxis(V3.UP, dt)
      @ring.rotateOnAxis(V3.UP, -dt)

      if @hovered
        r = dt * 0.5
        @inner.rotateOnAxis(V3.UP, -r)
        @top.rotateOnAxis(V3.UP, r)
        @windows.rotateOnAxis(V3.UP, -r)

    mouse : (dt, hit) ->
      if hit?
        if Mouse.pressed()
          if hit.object.name is "verseny_windows"
            range = s : 1.14, e : 4.08
            t = ilerp(range.s, range.e, hit.point.y)
            if t is 1 then t = 0.99
            n = Math.floor(t * 7)
            target = (Toner.bass.current_step + 2) % 16

            Toner.bass.pattern[target].note = n
          else if hit.object.name is "verseny_door"
            for i in [0..15]
              Toner.bass.pattern[i].note = null
            # console.log ilerp(hit.point.)


    hovers : ["verseny", "verseny_top","verseny_bottom","verseny_logo", "verseny_ring"]
    logo_offset : 0.7
    spread : (d) ->
      @pole.translateY @logo_offset * d
      @logo.translateY @logo_offset * d
      @ring.translateY @logo_offset * d
      @top.translateY 0.6 * d
      @inner.translateY 0.4 * d
      @windows.translateY 0.4 * d
    hover : () ->
      Mats.hover.color.setHex 0x26ABE2
      @windows.material = Mats.hover
      @spread 1
      @hovered = true

    leave : () ->
      @inner.setRotationFromQuaternion @start_rot
      @top.setRotationFromQuaternion @start_rot
      @bottom.setRotationFromQuaternion @start_rot
      @windows.setRotationFromQuaternion @start_rot
      @spread -1
      @windows.material = Mats.logos
      @hovered = false
      # @bottom.material = Mats.main

  gyik :
    page : new PageTransform(0)
    cam : {"pos":[-0.8146333668487787,4.520265065199937,1.5901020737110223],"pitch":0.19200000000000142,"yaw":-0.25199999999999945,"roll":0,"fov":14.036601356639087}

    init : (s) ->
      @object = s.getObjectByName "gyik"
      @left = s.getObjectByName "gyik_left"
      @right = s.getObjectByName "gyik_right"
      @eyes = 
        left : s.getObjectByName "eyeL"
        right : s.getObjectByName "eyeR"

      @raygroup = [s.getObjectByName("-gyik_left"), s.getObjectByName("-gyik_right")]

    update : (dt, hit) ->
      if hit?
        p = hit.point.clone()
        if p.z < -2 then p.z = -2
        if p.x < -4 then p.x = -4
        @eyes.right.lookAt(p)
        @eyes.left.lookAt(p)
    hover : () ->
      @object.material = Mats.emissive
      @left.material = Mats.emissive
      @right.material = Mats.emissive
    leave : () ->
      @object.material = Mats.main
      @left.material = Mats.main
      @right.material = Mats.main
    
    mouse : (dt, hit) ->
      if hit?
        if Mouse.pressed()
          if hit.object.name is "-gyik_right"
            range = s : 2.3, e : 6.1
            t = ilerp(range.s, range.e, hit.point.y)
            r = 1.8 + t * Math.PI * 0.7
            @right.rotation.set @right.rotation.x, @right.rotation.y, r
            Toner.bass.distortion.set( wet : 0.3 * t)
            Toner.bass.filter.set( frequency : 420 + 1420 * t)
          else if hit.object.name is "-gyik_left"
            range = s : 3.7, e : 4.9
            t = ilerp(range.s, range.e, hit.point.y)
            r = 2.64 + t * Math.PI * 0.2
            @left.rotation.set @right.rotation.x, @right.rotation.y, r
            Toner.chords.filter.set( frequency : 800 + 4000 * t)
            
            # Tone.Transport.set bpm : 81 + 110 * (t * t)

  kapcsolat : 
    page : new PageTransform(0, 0.1, 0.4)
    cam : {"pos":[3.445156386532102,12.356510245784168,-0.37926672934551386],"pitch":0.2931001630424697,"yaw":-1.170796326794893,"roll":0,"fov":15.717206240823533}


    init : (s) ->
      @object = s.getObjectByName "kapcsolat"
    hover : () ->
      Mats.hover.color.setHex 0xffffff
      @object.material = Mats.hover

    mouse : (dt, hit) ->


  nevezes : 
    page : new PageTransform()
    cam : {"pos":[3.2272101768167847,5.67259600391876,5.40482568631633],"pitch":0.09999999999999878,"yaw":-0.030000000000001213,"roll":0,"fov":15.42966422794606}
    init : (s) ->
      @object = s.getObjectByName "nevezes"
      @collider = s.getObjectByName "_nevezes"
      @object.material = Mats.sun
      @sun = new THREE.DirectionalLight()
      @sun.color.setHSL 0.1, 1, 0.8
      # @sun.color.setHSL 0.1, 1, 0.95 
      @sun.position.set 0, 3, -4
      @sun.lookAt 0,0,0
      @sun.position.multiplyScalar 30
      @sun.castShadow = true
      @sun.shadow.mapSize.width = 2048
      @sun.shadow.mapSize.height = 2048
      d = 10
      @sun.shadow.camera.left = - d
      @sun.shadow.camera.right = d
      @sun.shadow.camera.top = d
      @sun.shadow.camera.bottom = - d
      @sun.shadow.camera.near = 0
      @sun.shadow.camera.far = 155
      @sun.shadow.bias = 0.000001
      s.add @sun

      @sun_colors = new Gradient([new T.Color().setHSL(0, 0.8, 0.5), new T.Color().setHSL(0.2, 1, 0.7)])
      @amb_colors = new Gradient([new T.Color().setHSL(0, 0.6, 0.4), new T.Color().setHSL(0.2, 0.6, 0.6)])
      @sky_colors = new Gradient([new T.Color().setHSL(0.8, 0.7, 0.5), new T.Color().setHSL(0.3, 0.9, 0.95)])

      @t = 0.5
      @tt = Math.sqrt @t
      Mats.sun.color.set @sun_colors.at @t, true

      @ac = @amb_colors.at @t
      @ar = 0.8
      @a = 0.7
      @ambient = new T.AmbientLight()
      @ambient.color.copy @ac
      s.add @ambient
      
      # @sky = new T.Color().setHSL(0.5, 0.9, 0.8)
      @sky = @sky_colors.at @tt, true
      s.background = @sky

      @sun_track = s.getObjectByName "-sun_track"

      @raygroup = [@sun_track]
    hover : () ->
      @hovered = true
    leave : () ->
      @hovered = false
      @sun.intensity = 1
      @ambient.color.copy @amb_colors.at @t
      @ambient.intensity = 1
      Mats.emissive.color.setRGB 1, 1, 1

    update : (dt, hit) ->
      if @hovered and hit?
        p = hit.point.clone()
        p.setZ 0
        d = p.distanceTo @collider.position
        d = 1 - ilerp(0.18, 0.78, d - 10)
        # d = d * d * d
        c = 1 + d * 0.2
        Mats.emissive.color.setRGB c,c,c
        @sun.intensity = 1 - d * 0.7
        @ambient.intensity = 1 - d * 0.3
        @ac = @amb_colors.at @t
        @ambient.color.setRGB @ac.r, @ac.g - d * 0.1, @ac.b

    pressed : false
    down : false
    mouse : (dt, hit) ->
      if hit? and hit.object.name is "-sun_track"
        if Mouse.pressed()
          range = s : 3.4, e : 14
          t = ilerp(range.s, range.e, hit.point.y)
          if t is 1 then t = 0.99
          i = Math.floor(Toner.lead.scale_length * t)
          y = hit.point.y
          yy = clamp(y, range.s, range.e)
          @object.position.setY yy
          @sun.position.set 0, yy + 1, -4
          # @sun.position.multiplyScalar 30
          @sun.lookAt 0, 0, 0

          # tt = (1.0 / Toner.lead.notes.length) * i
          tt = Math.sqrt t
          @t = t
          @sun.color.copy @sun_colors.at t
          Mats.sun.color.set @sun_colors.at t, true
          # Mats.letters.color.set @sun_colors.at t, true
          @ambient.color.copy @amb_colors.at t
          World.scene.background = @sky_colors.at tt, true

          # Toner.filter.set frequency : 400 + 2000 * t
          Toner.lead.filter.set frequency : 400 + 4000 * t
          Toner.lead.startNote t
          @pressed = true
      if @pressed and not Mouse.pressed()
        @pressed = false
        Toner.lead.endNote()

  esemenyek : 
    page : new PageTransform(0)
    cam : {"pos":[1.2317666805179708,0.9014780409478149,4.227273677628121],"pitch":-0.7291853071795774,"yaw":-0.1139999999999977,"roll":0,"fov":14.22596528725142}

    init : (s) ->
      @object = s.getObjectByName "esemenyek"
      @deck = s.getObjectByName "esemenyek_deck"
      @y = @object.position.y
      @pole = s.getObjectByName "flagpole"
      @holders = s.getObjectByName "flag_holders"
      @holders.material = Mats.logos

      restDistance = 25
      xSegs = 10
      ySegs = 13
      gravity = new THREE.Vector3( 0, - GRAVITY, 0 ).multiplyScalar( MASS )
      pins = [ 0, 3, 6, 9 ]
      plane = ( width, height ) ->
        ( u, v, target ) ->
          x = ( u - 0.5 ) * width
          y = ( v + 0.5 ) * height
          z = 0
          target.set( x, y, z )
      clothFunction = plane( restDistance * xSegs, restDistance * ySegs)
      clothGeom = new THREE.ParametricBufferGeometry( clothFunction, xSegs, ySegs )
      clothGeom.attributes.uv.array.set(clothGeom.attributes.uv.array.map((v, i) -> if i % 2 is 0 then 0.99 else 0))
      @cloth = new Cloth( xSegs, ySegs, restDistance, clothFunction, gravity, pins, clothGeom)

      @flag = new THREE.Mesh( @cloth.geometry, Mats.logos )
      @flag.position.set( 0, 0, 0 )
      @flag.castShadow = true
      @flag.name = "esemenyek_flag"
      ss = 0.0034
      p = @pole.position.clone()
      p.setY p.y - 0.52
      @flag.position.copy p
      @flag.scale.set ss * 1.1, ss, ss
      @flag.rotation.copy @pole.rotation
      @flag.rotateY Math.PI * 0.5
      s.add @flag
      @wind = @idle_wind


      @top = s.getObjectByName "ship_top"
      @top.color = 0
      @left = s.getObjectByName "ship_left"
      @left.color = 1
      @right = s.getObjectByName "ship_right"
      @right.color = 2

      @raygroup = [@top, @left, @right]

      s = 1 / 8
      @colors = [
        [0, 0]
        [2, 1]
        [2, 2]
        [0, 3]
        [1, 3]
        [0, 4]
        [6, 0]
      ].map((c) -> [c[0] * s + s * 0.5, c[1] * s + s * 0.5])
    idle_wind : 0.3
    set_wind : (w, t) ->
      windStrength = Math.cos( t / 7000 ) * 20 * w + 40 * w
      @cloth.wind.force.set( Math.sin( t / 2000 ), Math.cos( t / 3000 ), Math.sin( t / 1000 ) )
      @cloth.wind.force.normalize()
      @cloth.wind.force.multiplyScalar( windStrength )
    update : (dt) ->
      time = Date.now()
      @set_wind @wind, time
      @cloth.simulate( time )
      p = @cloth.particles
      for i in [0..p.length - 1]
        v = p[ i ].position
        @cloth.geometry.attributes.position.setXYZ( i, v.x, v.y, v.z )
      @cloth.geometry.attributes.position.needsUpdate = true
      @cloth.geometry.computeVertexNormals()
    hover : () ->
      @wind = 2
      Mats.hover.color.setHex Colors.white
      @deck.material = Mats.white
      Mats.hover.side = T.DoubleSide
      Toner.ambience.filter.frequency.rampTo(2000, 0.6)
      Toner.ambience.volume.rampTo(-2, 0.6)
    leave : () ->
      @wind = @idle_wind
      @deck.material = Mats.main
      Mats.hover.side = T.FrontSide
      Toner.ambience.filter.frequency.rampTo(1000, 0.6)
      Toner.ambience.volume.rampTo(-10, 0.6)
    

    random_color_box : (b) ->
      b.color = (b.color + 1) % @colors.length
      c = @colors[b.color]
      uvs = new Float32Array( [0..b.geometry.attributes.uv.array.length - 1].map((n) -> 
        c[n % 2]
      ))
      b.geometry.addAttribute "uv", new THREE.BufferAttribute( uvs, 2 )


    pressed : false
    mouse : (dt, hit) ->
      if hit?
        if Mouse.pressed() and not @pressed
          @pressed = true
          if @raygroup.find((o) -> o.name is hit.object.name)?
            instrument = 
              switch hit.object.name
                when "ship_top" then "drums"
                when "ship_left" then "bass"
                when "ship_right" then "chords"
            @random_color_box hit.object
            Toner.try_unmute instrument
            Toner.randomize instrument


      if @pressed and not Mouse.pressed()
        @pressed = false
          
        


  szponzorok : 
    page : new PageTransform(0, 0.1, 0.4)

    cam : {"pos":[3.676299425333975,2.2379572043429814,4.597354252805076],"pitch":-0.8100000000000033,"yaw":0.02400000000000216,"roll":0,"fov":15.99414198915725}
    hovers : ["szponzorok", "logos"]
    init : (s) ->
      @object = s.getObjectByName "szponzorok"
      @logos = s.getObjectByName "logos"
      @logos.material = Mats.logos
      @billboard = new Billboard "logos", Logos
      @raygroup = [@logos]
      @instrument = Toner.drums
    update : (dt, hit) ->
      @billboard.update()

      if @billboard.dirty 
        @billboard.dirty = false
        @logos.material.map.needsUpdate = true

    pressed : false
    mouse : (dt, hit) ->
      if hit? and hit.object.name is "logos"
        rangey = 
          s : 0.24
          e : 0.89
        row = Math.floor(ilerpi(rangey.s, rangey.e, hit.uv.y) * Toner.drum_samples.length)
        col = Math.floor(hit.uv.x * 16)

        if Mouse.pressed()
          if not @pressed
            @pressed = true
            Toner.drums.pattern[row][col].note = !Toner.drums.pattern[row][col].note
        else if @pressed
          @pressed = false

    hover : () ->
      Mats.hover.color.setHex Colors.light_blue
      @object.material = Mats.hover
    leave : () ->
      @object.material = Mats.main
    click : () ->
      @billboard.show_seq = true
    exit : () ->
      @billboard.show_seq = false
      @billboard.draw_current()


  mute : 
    muted : true
    init : (s) ->
      @object = s.getObjectByName "mute"
      @info = s.getObjectByName "info"
      @info.visible = false
      @info.material = Mats.info
      @box = s.getObjectByName "mute_box"
      @posy = 0.3
      p = @object.position.clone()
      @object.parent = @box
      @object.rotation.set 0, 0, 0
      @object.position.set 0, @posy, 0
      @object.rotateX Math.PI

      @stop()

      if isMobile.any
        @posy = -1
        
    time : 0
    speed : 6
    update : (dt, hit) ->
      if @muted
        @time += dt
        @object.position.y = @posy + Math.sin(@time * @speed) * 0.03
    hovers : ["mute", "mute_box"]
    spread : (s) ->
      @box.translateY s
    hover : () ->
      @spread 0.3
      @info.visible = true
    leave : () ->
      @spread -0.3
      @info.visible = false
    start : () -> 
      Tone.context.resume()
      Tone.Transport.start()
      @object.rotation.set 0, @object.rotation.y, @object.rotation.z
      @muted = false
    stop : () ->
      Tone.Transport.stop()
      Tone.context.suspend()
      @object.rotation.set Math.PI, @object.rotation.y, @object.rotation.z
      @muted = true

    click : () ->
      if @muted and not isMobile.any
        @start()
      else
        @stop()

  nyertesek : 
    path : [
      {"pos":[7.444234453396267,10.088540289744824,-3.244837932553484],"pitch":-0.7480000000000026,"yaw":-0.33999999999999714,"roll":0,"fov":15.314807888272327},
      {"pos":[8.559773783561337,9.9566823049907,-3.4210010305146],"pitch":2.2844073464102076,"yaw":-0.7599999999999972,"roll":0,"fov":15.314807888272327},
      {"pos":[7.739251393693994,5.609856556297405,-1.961465635175792],"pitch":2.8724073464102077,"yaw":-0.911999999999997,"roll":0,"fov":15.314807888272327},
      {"pos":[6.577742928730362,2.7118582270438987,-1.0159062231200167],"pitch":2.8124073464102063,"yaw":-0.6659999999999968,"roll":0,"fov":15.314807888272327},
      {"pos":[3.8250884128325207,0.8758972369240792,1.1242870253484263],"pitch":2.82824073464102075,"yaw":-0.25199999999999734,"roll":0,"fov":15.314807888272327},
      {"pos":[3.7100451050236836,0.8758972369240792,2.738163130639654],"pitch":2.67440734641020778,"yaw":-0.22999999999999735,"roll":0,"fov":15.314807888272327},
      {"pos":[3.2729463282934708,0.8758972369240792,4.7506571563610684],"pitch":1.968407346410194,"yaw":0.18400000000000263,"roll":0,"fov":15.314807888272327},
      {"pos":[-0.38096531449234167,1.6552784348038327,4.7609403652303035],"pitch":1.2491853071795576,"yaw":0.4200000000000026,"roll":0,"fov":15.314807888272327},
      {"pos":[-3.0085405951161066,2.864013706267163,2.982247058738921],"pitch":0.7831853071795734,"yaw":0.05600000000000286,"roll":0,"fov":15.314807888272327},
      {"pos":[-4.873269220779204,1.8782617432340036,-0.07514036258519564],"pitch":0.10518530717957997,"yaw":-0.26799999999999746,"roll":0,"fov":15.314807888272327},
      {"pos":[-4.714480688907536,0.9700643596990347,-2.532762430811164],"pitch":-0.8148146928204087,"yaw":-0.07999999999999736,"roll":0,"fov":15.314807888272327},
      {"pos":[-1.326237629413095,0.7700652366982428,-4.5986356222871505],"pitch":-1.0608146928203985,"yaw":0.07400000000000238,"roll":0,"fov":15.314807888272327},
      {"pos":[1.862481614146723,0.7652984583168805,-6.537040685085723],"pitch":-0.8208146928204061,"yaw":-0.2779999999999998,"roll":0,"fov":15.314807888272327},
      {"pos":[2.2868646222232063,0.7652984583176845,-7.047533182241461],"pitch":-2.0427779607687213,"yaw":-0.5899999999999981,"roll":0,"fov":15.314807888272327},
      {"pos":[3.1530134470870026,0.7652984583176845,-5.787411459038456],"pitch":-3.2247779607693599,"yaw":-0.40599999999999803,"roll":0,"fov":15.314807888272327},
      {"pos":[3.376023886550947,0.015455067713725823,-3.4657915738694824],"pitch":-3.6632220392306277,"yaw":-0.041999999999996575,"roll":0,"fov":15.314807888272327},
      {"pos":[2.6319579974347223,0.01545483707039059,-1.2667919939787462],"pitch":-2.91922203923062353,"yaw":0.29400000000000304,"roll":0,"fov":15.314807888272327},
      {"pos":[1.0640951415478799,0.71545483707039059,1.9746790815957636],"pitch":-2.848777960769367516,"yaw":0.17600000000000293,"roll":0,"fov":15.314807888272327},
      {"pos":[1.5737432604235146,0.81545483707039059,4.525649796853341],"pitch":-0.8567779607693402,"yaw":0.21200000000000274,"roll":0,"fov":15.314807888272327},
      {"pos":[3.2173295023360167,0.9884327149519566,5.314169336672116],"pitch":-0.8028146928203972,"yaw":0.2760000000000026,"roll":0,"fov":15.314807888272327},
      {"pos":[4.1575018353614155,1.6886416966044264,4.546822811295116],"pitch":-0.5868146928204061,"yaw":0.2240000000000009,"roll":0,"fov":15.314807888272327},
      {"pos":[5.088247903209209,1.048650305649258,3.9882188484968863],"pitch":-0.46881469282041127,"yaw":0.19800000000000095,"roll":0,"fov":15.314807888272327},
      {"pos":[8.966979140705325,1.0391096413478644,-4.443303624069709],"pitch":-0.464248146928204128,"yaw":0.2300000000000009,"roll":0,"fov":15.314807888272327},
      {"pos":[8.966979140705325,1.0391096413478644,-4.443303624069709],"pitch":2.8352220392306184,"yaw":0.06800000000000099,"roll":0,"fov":15.314807888272327},
      {"pos":[7.056149146905438,1.048650305649258,-0.45727317079180735],"pitch":2.8392220392306138,"yaw":0.03800000000000108,"roll":0,"fov":15.314807888272327},
      {"pos":[6.208219602245835,1.048650305649258,0.4299504683014424],"pitch":1.3872220392304642,"yaw":-0.05999999999999896,"roll":0,"fov":15.314807888272327},
      {"pos":[5.24778216165286,1.048650305649258,0.22027809712731844],"pitch":1.0523706143590994,"yaw":0.08200000000000111,"roll":0,"fov":15.314807888272327},
      {"pos":[3.5687543901193886,1.3169347017089024,-0.7522701661735576],"pitch":1.0283706143591023,"yaw":0.07400000000000123,"roll":0,"fov":15.314807888272327},
      {"pos":[1.899018305209003,1.2486684444597034,-1.7324816484072245],"pitch":1.0483706143590985,"yaw":-0.005999999999998733,"roll":0,"fov":15.314807888272327},
      {"pos":[-0.22382133302994772,1.0446613652532442,-2.918839112394632],"pitch":1.4075799999999907,"yaw":-0.10672999999999963,"roll":0,"fov":15.314807888272327},
      {"pos":[-1.607022137893344,0.9173943302550372,-1.4401679600140143],"pitch":1.2623706143590574,"yaw":-0.06199999999999999,"roll":0,"fov":15.314807888272327},
      {"pos":[-2.0537406520777037,2.053635356963852,-1.5807994793056042],"pitch":1.2663706143590607,"yaw":0.44400000000000023,"roll":0,"fov":15.314807888272327},
      {"pos":[-2.0561879767042974,4.594639921471919,-1.5726583547098911],"pitch":1.2783706143590583,"yaw":0.6120000000000004,"roll":0,"fov":15.314807888272327},
      {"pos":[-1.994858072014878,5.921870003407559,-1.570988625336652],"pitch":1.3423706143590253,"yaw":-0.33,"roll":0,"fov":15.314807888272327},
      {"pos":[0.47073746345122824,6.539697342788252,-2.55169387702537],"pitch":1.327222039230496,"yaw":-0.4560000000000005,"roll":0,"fov":15.314807888272327},
      {"pos":[2.9559275591265015,6.539697342788252,-4.566862396914512],"pitch":1.1112220392305603,"yaw":-0.47000000000000036,"roll":0,"fov":15.314807888272327},
      {"pos":[5.168272811817883,10.012349077384815,-3.804856869319665],"pitch":1.079222039230566,"yaw":-0.8679999999999994,"roll":0,"fov":15.314807888272327}
      ].map((t) -> 
        t.view_offset = 0; t.fov = undefined; t.pos = new Vector3().fromArray t.pos; t)
    page : new PageTransform(0.15, 0.15, 0.7)
    cam : {"pos":[7.444234453396267,10.088540289744824,-3.244837932553484],"pitch":-0.7480000000000026,"yaw":-0.33999999999999714,"roll":0,"fov":15.314807888272327},
    hovers : ["nyertesek"]
    opened : false
    create_letter : (l) ->
    set_screen : (x,y) ->
      c = [x,y]
      uvs = new Float32Array( [0..@screen.geometry.attributes.uv.array.length - 1].map((n) -> 
        c[n % 2]
      ))
      @screen.geometry.addAttribute "uv", new THREE.BufferAttribute( uvs, 2 )
    init : (s) ->
      @scene = s
      @laptop = s.getObjectByName "nyertesek_laptop"
      @laptop_y = @laptop.position.y
      @screen = s.getObjectByName "nyertesek_screen"
      @box = s.getObjectByName "nyertesek_box"
      @poles = s.getObjectByName "nyertesek_poles"
      @screen.material = Mats.logos
      if NYERTESEK
        loaders.font.load( 'font/Rubik-Medium.json', ((font) =>
          t = "NYERTESEK".split("")
          @text = new T.Group()
          @text.position.copy(s.getObjectByName("nyertesek_screen").position)
          @text.rotation.copy(s.getObjectByName("nyertesek_screen").rotation)
          @text.translateX(-2.1)
          @text.translateY(-0.15)

          @letters = t.map((l, i) =>
            geometry = new T.TextGeometry(l, 
              font : font
              size : 0.6
              height : 0.2
              curveSegments : 4
            )
            mesh = new T.Mesh(geometry, Mats.letters)
            @text.add mesh
            mesh.translateX i * 0.46
            mesh
          )
          s.add(@text)
        )
        )
        @object = s.getObjectByName "nyertesek"
        @raygroup = []
        @current = 0
        @next = 0
        @clock = new Clock()
      else
        @laptop.visible = false
        @screen.visible = false
        @box.visible = false
        @poles.visible = false
        s.remove s.getObjectByName "_nyertesek"
    lerp_color : (a, b, t) ->
      _a = new T.Color().copy a
      _a.lerp(b, t)
    update : (dt, hit) ->
      if @letters?
        speed = if @hovered then 0.01 else 0.001
        t = 0.5 + 0.42 * Math.sin(new Date().getTime() * speed)

        @laptop.position.y = @laptop_y + t * 0.2
        @laptop.rotation.y = - 1 + t
        Mats.letters.color.copy @lerp_color(Mats.sun.color, Colors.black, t)

        o = 0.08
        for l, i in @letters
          l.position.y = Math.sin(new Date().getTime() * 0.001 + Math.PI * 2 * (i / @letters.length)) * o

      if @opened and Main.world.cam_clock.state isnt RUNNING
        if @clock.update(dt)
          @current = wrap(@current + 1, @path)
          @next = wrap(@current + 1, @path)
          distance = @path[@current].pos.distanceTo(@path[@next].pos) + Math.abs(@path[@current].pitch - @path[@next].pitch)
          
          @clock.speed = 
            if @distance isnt 0 then 1 / distance
            else 0.1
          # console.log @path[@current].pos, @current
        a = @path[@current]
        b = @path[@next]
        Main.world.lerp_cam(a, b, @clock.time)
    mouse : (dt, hit) ->
    hover : () ->
      @hovered = true
      @screen.material = Mats.hover
      Mats.hover.color = new T.Color Colors.red
    leave : () ->
      @hovered = false
      @screen.material = Mats.logos
    click : () ->
      if NYERTESEK
        @opened = true
        @current = 0
        @clock.reset()
        @clock.speed = 0.12
        @next = wrap(@current + 1, @path)
    exit : () ->
      @opened = false

  world : 
    cam : "pos":[0.3519096374090766,3.7649676900074187,6.793668572125913],"pitch":-0.06400000000000003,"yaw":(if isMobile.any then 0 else -0.15),"roll":0,"fov":14.839657584438076, view_offset : 0
    init : (s) ->

for k of Objects
  if Objects[k].cam?
    Objects[k].cam.pos = new Vector3().fromArray Objects[k].cam.pos
  if not Objects[k].raygroup?
    Objects[k].raygroup = []

if not NYERTESEK
  Objects.cleaner = Objects.nyertesek
  Pages.pop()
  delete Objects.nyertesek
