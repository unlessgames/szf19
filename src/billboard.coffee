class Billboard
  constructor : (id, @logos) ->
    @ctx = $("#logos").get(0).getContext( '2d' )
    @ctx.canvas.width = 512
    @ctx.canvas.height = 512
    @w = 512
    @h = 448
    @current = 0
    @speed = 0.01
    @time = 1
    @images = []
    @clear_color = "#eeed"

    for l in @logos
      img = new Image()
      img.src = "#{l.base_dir}/#{l.name}"
      l.img = img
      # img.onload = (() -> @draw(l)).bind @
      @images.push img


    @seq =
      x : 0
      y : 52
      w : 512
      h : 342
    @bass =
      x : 0
      y : @h
      w : 64
      h : 64
    @show_seq = false
  draw : (l) ->
    aspect = l.img.height / l.img.width
    @ctx.clearRect 0, 0, @w, @h
    @ctx.fillStyle = if l.color? then l.color else @clear_color
    @ctx.fillRect 0, 0, @w, @h
    @ctx.fillRect 448, @h, 64, 64
    # @ctx.fill()

    @ctx.fillStyle = "#eeea"
    @ctx.fillRect 0, 0, 1, 1
    # @ctx.fill()
    @ctx.drawImage(l.img, l.x * @w + 1,l.y * @h + 1, @w * l.size - 1, @h * aspect * l.size - 1)
    @ctx.fillStyle = '#222'
    @ctx.fillRect @bass.x, @bass.y, @bass.w, @bass.h
  draw_seq : (pattern, step) ->
    if Main.dom.current? and Main.dom.current.id is "szponzorok"
      @ctx.clearRect 0, 0, @w, @h
      @ctx.fillStyle = @clear_color
      @ctx.fillRect 0, 0, @w, @h
      # @ctx.fill()
      ww = (@seq.w / 16)
      hh = (@seq.h / pattern.length)

      @ctx.fillStyle = "#4446"
      @ctx.fillRect 0, 0, @w * 0.25, @h
      @ctx.fillRect @w * 0.5, 0, @w * 0.25, @h


      @ctx.fillStyle = "#d42d"
      x = @seq.x + step * ww
      y = @seq.y
      @ctx.fillRect x, y, ww, @seq.h

      for p, row in pattern
        y = @seq.y + (pattern.length - 1 - row) * hh
        for s, col in p
          if s.note
            x = @seq.x + col * ww
            @ctx.fillStyle = if s.note then (if step is col then "#fe2" else "#444") else "#999"
            @ctx.fillRect x + 1, y + 1, ww - 2, hh - 2
          # @ctx.fill()
          # console.log x, y, ww, hh
      @ctx.fillStyle = "#d42d"
      x = @seq.x + step * ww
      y = @seq.y
      @ctx.fillRect x, y, ww, @seq.h
      @dirty = true

  draw_bass : (pattern) ->
    # @ctx.clearRect 0, 0, @w, @h
    @ctx.fillStyle = '#222'
    @ctx.fillRect @bass.x, @bass.y, @bass.w, @bass.h
    # @ctx.fill()

    start = Toner.bass.current_step - 2


    ww = @bass.w / 7
    hh = 64 / 7
    for i in [0..6]
      x = @bass.x + i * ww
      ii = if start < 0 then 16 + start + i else start + i
      ii = ii % 16
      n = pattern[ii]
      if n.note?
        y = @bass.y + n.note * hh
        @ctx.fillStyle = if n.id is (start) then "#da2" else "#d42" 
        @ctx.fillRect x, y, ww, hh

    @dirty = true    

  draw_current : () ->
    @draw @logos[@current]
  update : () ->
      @time = @time + @speed
      if @time >= 1
        @current = (@current + 1) % @logos.length
        if not Main.dom.current? or (Main.dom.current.id isnt "szponzorok")
          @draw_current()
        else
          @draw_seq Toner.drums.pattern, Toner.drums.current_step
        @time -= Math.floor @time
        @dirty = true
