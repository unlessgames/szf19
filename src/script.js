// Generated by CoffeeScript 2.4.1
var Main;

Main = {
  is_mobile: false,
  resize: function() {
    this.dom.resize_page();
    return this.world.resize();
  },
  init: function() {
    this.dom = new Domer(this);
    this.toner = Toner;
    this.toner.init();
    if (!WEBGL.isWebGLAvailable()) {
      document.body.appendChild(WEBGL.getWebGLErrorMessage());
    } else {
      this.world = World;
      this.world.init(this.dom);
    }
    return $(window).resize(this.resize.bind(this));
  },
  open: function(id) {
    this.dom.open_page(id);
    return this.world.show_obj(id);
  }
};

Main.init();

//# sourceMappingURL=script.js.map
