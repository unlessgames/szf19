Toner = 
  init : () ->
    @dir = "./assets/sound/"
    notes = "C,db,D,eb,E,F,gb,G,ab,A,bb,B".split(",")
    @drum_samples = [
      "kick_a"
      "kick_b"
      "snare_a"
      "snare_b"
      "tom_a"
      "hihat_a"
      "hihat_b"
      "fx_a"
      "fx_b"
    ].map((d) => "#{@dir}drums/#{d}.mp3")

    drums = {}
    @drum_samples.map((d, i) -> drums[notes[i]] = d)

    @drums = new Tone.Players(drums, {
      "volume" : -10,
      "fadeOut" : "64n",
    })


    @drums.pattern = @drum_samples.map((d) -> [0..15].map((n) -> {note : Math.random() < 0.1, id : n}))
    @drums.pattern[0].forEach((n, i) -> n.note = i % 4 is 0)
    @drums.pattern[1].forEach((n) -> n.note = false)
    @drums.step = (time, e) ->
      @current_step = e
      @pattern.map((p, i) =>
        if p[e].note
          @get(notes[i]).start(time, 0, "16n", 0, 0.8)
      )
      Tone.Draw.schedule((() =>
        Objects.szponzorok.billboard.draw_seq @pattern, e
      ), time)
    @seq = new Tone.Sequence(@drums.step.bind(@drums), [0..15], "16n").start(0)

    @compressor = new Tone.Compressor().connect Tone.Master
    @drums.compressor = new Tone.Compressor().connect Tone.Master
    @drums.connect @compressor.threshold

    @drums.connect @drums.compressor


    # @reverb = new Tone.Reverb(3).toMaster()
    # @reverb.generate()
    @reverb = new Tone.Freeverb(roomSize : 0.3, wet : 0.5).toMaster()
    @reverb.dampening.value = 1420
    # @filter = new Tone.Filter(800, "bandpass")
    # @filter.Q = 0.5
    # @filter.rolloff = -12
    class Notez
      constructor : (@chord, bs, ls) ->
        @lead = ls.toLowerCase().split(",")
        @bass = bs.toLowerCase().split(",")
      get_bass_buffer_id : (n) ->
        r = Tonal.Note.midi(@bass[n]) - 36
        # console.log r
        r



    @notez = [
      new Notez("FM", 
        "f2,a2,bb2,c3,e3,f3,c4"
        "d4,e4,f4,g4,a#4,d5,e5,f5,g5,a#5,d6"
      )
      new Notez("GM", 
        "g2,b2,d3,e3,gb3,a3,b3"
        "d4,f#4,a4,a#4,c5,d5,e5,f#5,g5,a5,d6"
      )
      new Notez("AsM", 
        "d2,g2,bb2,d3,f3,g3,a3"
        "a#3,d4,f4,a#4,c5,d5,d#5,f5,g5,a#5,c6"
      )
      new Notez("CMi", 
        "c2,e2,f2,c3,e3,g3,c4"
        "d4,e4,f4,g4,a#4,d5,e5,f5,g5,a#5,d6"
      )
      new Notez("DsM",
        "eb2,g2,bb2,eb3,f3,g3,bb3"
        "a#3,d#4,g4,a#4,d5,d#5,f5,g5,g5,a#5,d#6"
      )
      # new Notez("Fsm", 
      #   "gb2,a2,b2,c3,d3,e3,gb3"
      #   "c3,db3,d3,eb3,e3,f3,gb3,g3,ab3,a3,bb3,b3"
      # )
    ]

    @lead = new Tone.DuoSynth(
      vibratoAmount : 0.4
      vibratoRate : 1
      portamento : 0.06
      harmonicity : 1.008
      volume : -12
      voice0 : 
        volume : -2
        oscillator : 
          "type" : "triangle"
        filter : 
          Q : 2
          type : "highpass"
          rolloff : -12
        
        envelope : 
          attack : 0.01
          decay : 0.25
          sustain : 0.4
          release : 1.2
        
        filterEnvelope : 
          attack : 0.001
          decay : 0.05
          sustain : 0.3
          release : 2
          baseFrequency : 420
          octaves : 4

      voice1 : 
        volume : -8
        oscillator : 
          type : "square"
        
        filter : 
          Q : 1
          type : "bandpass"
          rolloff : -12
        
        envelope : 
          attack : 0.1
          decay : 4
          sustain : 0.1
          release : 0.8
        
        filterEnvelope : 
          attack : 0.05
          decay : 0.05
          sustain : 0.1
          release : 2
          baseFrequency : 1000
          "octaves" : -1
    )
    # @lead = new Tone.PluckSynth()

    oct = 4
    @lead.delay = new Tone.FeedbackDelay("16n", 0.85)
    # @lead.notes = ["D4", "E4", "F4", "G4", "A#4", "D5", "E5", "F5", "G5", "A#5", "D6"]
    @lead.set attack : "16n"
    @lead.scale_length = 11
    @lead.startNote = (t) ->
      nt = Math.floor(t * @scale_length)
      if @current_note isnt nt
        @current_note = nt
        @triggerAttack(Toner.current_chord.lead[@current_note])
    @lead.endNote = () ->
      if @current_note?
        @triggerRelease()
        @current_note = null
    @lead.filter = new Tone.Filter(frequency : 1000, type : "bandpass", rolloff : -12, Q : 2)
    @lead.chain @lead.filter, @lead.delay, Tone.Master
    @lead.chain Tone.Master

    # @chords.chorus = new Tone.Chorus(frequency : 8, depth : 0.2, wet : 0.4, spread : 180)

    @bass_samples = [36..60].map((n) -> Tonal.Note.fromMidi(n).toLowerCase())
    @current_chord = @notez[0]
    @chords = new Tone.Player()
    @chords.notes = @notez.map((n) -> n.chord)

    @loaded_buffers = 0
    @all_buffers = @chords.notes.length + @bass_samples.length # + @drum_samples.length

    @load = () ->
      @loaded_buffers++
      if @loaded_buffers is @all_buffers
        @loaded = true

    @chords.pattern = [0..15].map( 
      (n) => 
        note : if n % 4 is 0 then Math.floor(Math.random() * @chords.notes.length) else @chords.notes.length
        id : n
        length : "4m"
    )
    @chords.step = (time, n) ->
      e = @pattern[n]
      if e.note < @notes.length
        Toner.current_chord = Toner.notez[e.note]
        # console.log Toner.current_chord
        @stop()

        @buffer = @buffers[e.note]
        @start()
    @chords.seq = new Tone.Sequence(@chords.step.bind(@chords), [0..15], "2n").start(0)
    @chords.buffers = @chords.notes.map((c) => new Tone.Buffer("#{@dir}/chords/#{c}.mp3", @load.bind(@)))
    @chords.set volume : 0
    @chords.filter = new Tone.Filter(type : "lowpass", frequency : 2200, rolloff : -12)
    @chords.fx = new Tone.Chorus("4n", 0.6)
    @chords.fx.wet.set 0.6
    @chords.chain @chords.filter, @chords.fx, @reverb, Tone.Master

    @bass = new Tone.Player()
    @bass.env = new Tone.AmplitudeEnvelope( 
      "attack": 0.001,
      "decay": 0.2,
      "sustain": 0.3,
      "release": 0.8
    )

    # @bass.notes = [5,7,9,10,12,14,24]
    # @bass.notes = [5,7,9,10,12,14,16,17,19,21,22,24]

    @bass.pattern = [0..15].map( 
      (n) => 
        if Math.random() < 0.2
          note : null #Math.floor(Math.random() * 7)#ranfrom @bass.notes
          id : n
        else 
          id : n
          note : null
    )
    @bass.step = (time, e) ->
      @current_step = e
      n = @pattern[e].note
      if n?
        @stop()
        @buffer = @buffers[Toner.current_chord.get_bass_buffer_id n]
        if @buffer.loaded
          @start()
          @env.triggerAttackRelease "8n"
      Tone.Draw.schedule((() =>
        Objects.szponzorok.billboard.draw_bass @pattern
      ), time )


    @bass.seq = new Tone.Sequence(@bass.step.bind(@bass), [0..15], "8n").start()
    @bass.filter = new Tone.Filter(frequency : 600, type : "lowpass", Q : 6, rolloff : -24)
    @bass.distortion = new Tone.BitCrusher(bits : 5, wet : 0.1)
    @bass.set volume : -6
    @bass.chain @bass.env, @bass.distortion, @bass.filter, Tone.Master

    @bass.buffers = @bass_samples.map((c) => new Tone.Buffer("#{@dir}/bass/#{c}.mp3", @load.bind(@)))

    @ambience = new Tone.Player("#{@dir}ambience.mp3")
    @ambience.filter = new Tone.Filter(type : "bandpass", frequency : 1000)
    @ambience.set volume : -10
    @ambience.loop = true
    @ambience.autostart = true
    @ambience.chain @ambience.filter, Tone.Master

    # @ambience.start()
    @drums.mute = true
    @bass.mute = true
    @chords.mute = true
    # @chords.mute = true
    Tone.context.suspend()
    Tone.Transport.setLoopPoints(0, "16m")
    Tone.Transport.loop = true
    Tone.Transport.bpm = 119

  try_unmute : (instrument) ->
    if @[instrument].mute
      @[instrument].mute = false

  randomize : (instrument) ->
    switch instrument
      when "drums"
        @drums.pattern = @drum_samples.map((d) -> [0..15].map((n) -> {note : Math.random() < 0.1, id : n}))
        @drums.pattern[0].forEach((n, i) -> n.note = i % 4 is 0)
        @drums.pattern[1].forEach((n) -> n.note = false)
      when "chords"
        @chords.pattern = [0..15].map( 
          (n) => 
            note : if n % 4 is 0 then Math.floor(Math.random() * @chords.notes.length) else @chords.notes.length
            id : n
            length : "4m"
        )
      when "bass"
        @bass.pattern = [0..15].map( 
          (n) => 
            note : if Math.random() < 0.3 then Math.floor(Math.random() * 7) else null
            id : n
        )
    
    
