const T = THREE
const Vector3 = T.Vector3
const Vector2 = T.Vector2
const Quat = T.Quaternion

const V3 = {
  ZERO : new T.Vector3(0,0,0),
  ONE : new T.Vector3(1,1,1),
  LEFT : new T.Vector3(-1,0,0),
  RIGHT : new T.Vector3(1,0,0),
  UP : new T.Vector3(0,1,0),
  DOWN : new T.Vector3(0,-1,0),
  FRONT : new T.Vector3(0,0,1),
  BACK : new T.Vector3(0,0,-1)
}

const Q = {
  IDENTITY : new Quat(),
  X : new Quat(1,0,0,0),
  Y : new Quat(0,1,0,0),
  Z : new Quat(0,0,1,0),
  XP90 : new Quat(Math.sqrt05,0,0,Math.sqrt05),
  YP90 : new Quat(0,Math.sqrt05,0,Math.sqrt05),
  ZP90 : new Quat(0,0,Math.sqrt05,Math.sqrt05),
  XM90 : new Quat(-Math.sqrt05,0,0,Math.sqrt05),
  YM90 : new Quat(0,-Math.sqrt05,0,Math.sqrt05),
  ZM90 : new Quat(0,0,-Math.sqrt05,Math.sqrt05)
}
