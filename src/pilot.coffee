class Pilot 
  viewspeed : 0.00004
  movespeed : 40
  perspcam : new T.PerspectiveCamera(20, innerWidth / innerHeight, 0.01, 1000)
  camera : null
  height : 2
  fov_speed : - 0.03
  dirs :
    "KeyW" : V3.FRONT
    "KeyS" : V3.BACK
    "KeyA" : V3.LEFT
    "KeyD" : V3.RIGHT
    "KeyQ" : V3.DOWN
    "Space" : V3.UP
  keys_to_dir :
    "KeyW" : "forward"
    "KeyS" : "backward"
    "KeyA" : "left"
    "KeyD" : "right"
    "KeyQ" : "down"
    "Space" : "up"
  views : []
  active : false
  current_view : 0
  controls : null
  direction : new Vector3(0,0,0)
  velocity : new Vector3(0,0,0)
  curve : new THREE.CatmullRomCurve3([], true, "catmullrom", 0.5)
  add_view : () ->
    t = @get_camera_transform()
    @views.push(t)
  print_views : () -> JSON.stringify @views
  move : 
    forward : false
    backward : false
    left : false
    right : false
    up : false
    down : false
  clock : new Clock(1)
  get_transform : () ->
    @camera.updateMatrixWorld()
    matrix : @camera.matrixWorld.toArray()
    fov : @camera.getFocalLength()
  get_camera_transform : () ->
    pos : @controls.getObject().position.toArray()
    pitch : @controls.getObject().rotation.y
    yaw : @controls.getObject().children[0].rotation.x
    roll : 0
    fov : @camera.getFocalLength()

  print_transform : () ->
    JSON.stringify(@get_camera_transform())
  print : () -> 
    console.log @print_transform()
  fly : (dt) ->
    @velocity.x -= @velocity.x * 10 * dt
    @velocity.y -= @velocity.y * 10 * dt
    @velocity.z -= @velocity.z * 10 * dt
    @direction.x = Number( @move.left ) - Number( @move.right )
    @direction.y = Number( @move.down ) - Number(@move.up)
    @direction.z = Number( @move.forward ) - Number( @move.backward )
    @direction.normalize()

    if @move.forward or @move.backward then @velocity.z -= @direction.z * @movespeed * dt * (if Keys.shift() then 2 else 1 )
    if @move.left or @move.right then @velocity.x -= @direction.x * @movespeed * dt * (if Keys.shift() then 2 else 1 )
    if @move.up or @move.down then @velocity.y -= @direction.y * @movespeed * dt * (if Keys.shift() then 2 else 1 )

    obj = @controls.getObject()
    obj.translateX( @velocity.x * dt )
    obj.translateY( @velocity.y * dt )
    obj.translateZ( @velocity.z * dt )
  fov : 20
  goal_transform : null
  set_fov : (zoom) ->
    @fov *= 1 + (if zoom then - @fov_speed else @fov_speed )
    @camera.fov = @fov
    @camera.updateProjectionMatrix()
  onscroll : (e) ->   
    @set_fov(e.deltaY > 0)

  start_look : () -> @active = true; @controls.lock() 
  end_look : () -> @active = false; document.exitPointerLock()

  start_move : (e) ->
    if @keys_to_dir[e]?
      @move[@keys_to_dir[e]] = true
  end_move : (e) ->
    if @keys_to_dir[e]?
      @move[@keys_to_dir[e]] = false
  
  reset_cam : () ->
  
  set_transform : (t) ->
    cobj = @controls.getObject()
    if cobj?
      cobj.position.fromArray t.pos
      cobj.rotation.set(0, t.pitch, 0)
      cobj.children[0].rotation.set(t.yaw, 0, 0)
      @camera.setFocalLength t.fov
  moveTowards : (x, t) ->
    @set_transform @camera_transform.lerpTo(x, t)

  update : (dt) ->
    # @fly dt
  constructor : (scene) ->
    # Keys.listen @, "add_view", "press", "KeyV"
    # Keys.listen @, "print_views", "press", "KeyB"
    # Keys.listen @, "print", "press", "KeyP"
    # Keys.listen(@, "start_move", "press", "all")
    # Keys.listen(@, "end_move", "release", "all")
    # Mouse.listen(@, "onscroll", "scroll", "all")

    # Mouse.listen @, "start_look", "press", MOUSE_RIGHT
    # Mouse.listen @, "end_look", "release", MOUSE_RIGHT

    @camera = @perspcam
    @onscroll(true)
    @controls = new T.PointerLockControls(@camera)
    cobj = @controls.getObject()
    cobj._name = "_camera"
    cobj.name = "_camera"
    
    @scene = scene
    @scene.add cobj
    cobj
