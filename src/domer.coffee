class Domer
  constructor : (@main) ->
    @iframe = $("<iframe>", class : "iframe").attr("scrolling", "no").get(0)
    $("#page").appendTo($("body"))
    $("#close").on "mousedown", () => @close_page()
    $("#previous").click(() => @step_pages(-1))
    $("#next").click(() => @step_pages(1))
    $("#left, #title").click () => 
      if not @current? then @open_page "verseny"
      else if @current.id is "nyertesek"
        @current_index = 0
        @step_pages(0)
    $(".simplebar-content").html @iframe
    @inside_nyertes = false
  hover_page : (p) ->
    if p? and not @current?
      $("#title").html p.title
      # $("#left").css(display : "inline-block")
      $("#left").html p.button
    # else
    #   $("#left").css display : "none"

  set_title : (p) ->
    if @current?
      $("#title").html p.title
      $("#left").html p.button

  resize_page : () ->
    doc = @iframe.contentWindow.document
    body = doc.body
    html = doc.documentElement
    h = Math.max( 
      body.scrollHeight, body.offsetHeight, 
      html.clientHeight, html.scrollHeight, html.offsetHeight )
    # h = @iframe.contentDocument.getElementsByClassName("pages")[0].scrollHeight
    h = height : "#{h + (if isMobile.any then 100 else 10)}px"
    $("iframe").css  h 
    $(".simplebar-content-wrapper").css  h 



  show_page : () ->
    $("#page").animate(opacity : 1, top : $("#titlebar").height(), 320)

  active : (n) ->
    @current? and @current.id is n

  open_url : (url, menu) ->
    if menu? or not @current?
      @open_page (if menu? then menu else "nevezes"), false, url
    else
      $(@iframe).attr("src", url)
      $(".simplebar-content-wrapper").scrollTop 0  
      $("iframe").css height : 0 
      @iframe.onload = (() =>
        @resize_page()
      )

  open_page : (n, from_step, src) ->
    p = get_page n


    $("#title").html p.title
    $("#left").css(display : "inline-block").html p.button
    @current = p
    @current_index = Pages.findIndex((pp) -> pp.id is p.id)
    
    if p.id is "nyertesek"
      @current_index = 0

    if not src?
      src = if p.subpages? then p.subpages[0] else p.source
    $(@iframe).attr("src", src)
    @set_title p
    if innerWidth / innerHeight < 1 or isMobile.any
      $("#page").css top : 0, width : "100vw", left : 0, display : "block"
    else
      w = 100 * Objects[p.id].page.w
      $("#page").css(top : "100vh", width : "#{w}vw", display:"block")
    $(".simplebar-content-wrapper").scrollTop 0  
    $("iframe").css height : 0 
    @iframe.onload = (() =>
      @resize_page()
    )
    @show_page()
    if not from_step
      Main.world.show_obj p.id


  close_page : (cb, keep_bar) ->
    if @current.id is "nyertesek"
      @current_index = 0
    if @current and Objects[@current.id].exit?
      Objects[@current.id].exit()
    @current = null
    $("#page").animate(opacity: 0, top : "100vh", 420, (e) ->
      $(this).css(width : 0, display : "none")
      if cb? then cb(e)
    )
    if not keep_bar
      Main.world.show_obj("menu")


  step_pages : (d) ->
    if Main.world.cam_clock.state is 0
      if @current?
        if @current.id is "nyertesek"
          @current_index = wrap(@current_index + d, @current.subpages)
          src = @current.subpages[@current_index]
          $(@iframe).attr("src", src)
        else
          next = Pages[wrap(@current_index + d, Pages)]
          Main.world.show_obj next.id, true
          @close_page((() => @open_page(next.id, true)), true)
      else
        @open_page "verseny"
        Main.world.show_obj "verseny"
  
  random_content : () ->
    new Array(100).fill(0).map(()-> Math.ranDom() * 1000000).join("<br>")
  
  last : null

  update : (dt, hit, h) ->
    if hit? and (hit.object.name[0] isnt "-" and (NYERTESEK or hit.object.name isnt "_nyertesek"))
      $("#main").css(cursor : "pointer")
    else
      $("#main").css(cursor : "default")

    if h?
      if @last isnt h
        @hover_page get_page h
    else
      if not @current? and Main.world.cam_clock.state is 0
        $("#title").html Title
        # $("#left").css display : "none"
        $("#left").html "<19"

    @last = h
